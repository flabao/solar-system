using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusCamera : MonoBehaviour {
    
    private GameObject objectToFocus;       //private variable to store a reference to the game object


    private Vector3 offset;         //Private variable to store the offset distance between the player and camera
    private bool isFocusOn = false; //Private variable to store boolean. When true, camera will focus on object

    // Use this for initialization
    void Start()
    {
        //Calculate and store the offset value by getting the distance between the planet's position and camera's position.
        offset = new Vector3(0,25f,150f);
    }

    // Update is called after Update each frame
    // Continuously wait for planet to be tapped so that it can change its transform to look at desired planet
    // When SetFocus object is called in InteractManager, the if() statement will run
    void Update()
    {
        if (isFocusOn && objectToFocus != null)
        {
            //Change FocusCamera position and rotation to look at planet of focus + offset
            transform.position = objectToFocus.transform.position + offset;
            transform.LookAt(objectToFocus.transform);
        }
    }


    // Call function when InteractManager (raycast) clicks on a planet
    // Helper function to set FocusCamera transform to look at clicked planet
    // Set bool isFocusOn to true and objectToFocus != null to be able to run if() statement in update
    public void SetFocusObject(GameObject POIObject)
    {
        isFocusOn = true;
        objectToFocus = POIObject;
    }

}
